package com.example.threadasync.network

import io.reactivex.Observable
import retrofit2.http.GET
import com.example.threadasync.network.RetroCrypto

interface GetData {
    //to get API
    @GET("prices?key=e6e2d1dcc6fa596c7ff1d34c52b9fd45")
    fun getData() : Observable<List<RetroCrypto>>
}